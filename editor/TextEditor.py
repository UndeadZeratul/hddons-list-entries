import curses
from curses.textpad import Textbox
from typing import Callable

class TextEditor(Textbox):
	def __init__(self, win: curses.window, contents: str=""):
		super().__init__(win)
		self.og_contents = contents
		self.contents = contents
		self.curs_pos = [0, len(self.contents)] # y, x
		self.update_display()

	def update_display(self) -> None:
		offset = min(len(self.contents), self.curs_pos[1] - int(self.win.getmaxyx()[1] * 0.75))
		offset = max(0, offset)
		s = self.contents[offset:min(len(self.contents), offset + self.win.getmaxyx()[1] - 1)]
		if offset != 0:
			s = "<" + s[1:]

		if offset + self.win.getmaxyx()[1] - 1 < len(self.contents):
			s = s[:-1] + ">"

		self.win.clear()
		self.win.addstr(s)
		self.win.refresh()
		self.win.move(0, self.curs_pos[1] - offset)

	def do_command(self, ch: int) -> bool:
		if curses.ascii.isprint(ch):
			if self.curs_pos[1] == len(self.contents):
				self.contents = self.contents + chr(ch)

			else:
				self.contents = self.contents[:self.curs_pos[1]] + chr(ch) + self.contents[self.curs_pos[1]:]

			self.curs_pos[1] += 1

		elif ch in (curses.KEY_LEFT, curses.KEY_BACKSPACE, curses.ascii.DEL):
			if ch in (curses.KEY_BACKSPACE, curses.ascii.DEL):
				if (self.curs_pos[1] == 0):
					self.contents = self.contents[1:]

				else:
					self.contents = self.contents[:self.curs_pos[1] - 1] + self.contents[self.curs_pos[1]:]

			self.curs_pos[1] = max(0, self.curs_pos[1] - 1)

		elif ch == curses.KEY_RIGHT:
			self.curs_pos[1] = min(len(self.contents), self.curs_pos[1] + 1)

		elif ch in (curses.ascii.SOH, curses.KEY_HOME):
			self.curs_pos[1] = 0

		elif ch in (curses.ascii.ENQ, curses.KEY_END):
			self.curs_pos[1] = len(self.contents)

		elif ch in (curses.KEY_ENTER, curses.ascii.NL):
			return False

		elif ch in (curses.ascii.ESC, curses.ascii.BEL):
			self.contents = self.og_contents
			self.curs_pos[1] = len(self.contents)
			return False

		return True

	def gather(self) -> str:
		return self.contents

	def edit(self, validator: Callable=None) -> str:
		while True:
			ch = self.win.getch()
			if validator:
				ch = validator(self, ch)

			if not ch:
				continue

			if not self.do_command(ch):
				break

			self.update_display()

		return self.gather()
