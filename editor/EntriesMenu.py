import editor
from . Entry import Entry
from . Windows import Windows
from . BaseMenu import BaseMenu
from . EntryDetailsMenu import EntryDetailsMenu
from . TagsMenu import TagsMenu
from os.path import join

class EntriesMenu(BaseMenu):
    def __init__(self, win: Windows):
        super().__init__(win)

        self.keybinds = editor.DEFAULT_KEYBINDS + "   | [/] filters   | [r] reload"
        self.tags = []
        self.selected_tags = []
        self.entries = []
        self.search = ""

        self.win.clear_all()
        self.win.top.addstr("[Select entry]")
        self.win.bot.addstr(self.keybinds)
        self.win.refresh_all()

        self.update_entries()

        self.win.mid.clear()
        self.win.mid.addstr("Loading entries...\n(sorting tags...)")
        self.win.mid.refresh()

        self.tags.sort()

    # note: you have to clear entries and/or tags if you want to re-add them
    def update_entries(self) -> None:
        self.sel = 0
        self.entries.clear()
        self.lst.clear()
        self.win.mid.clear()
        self.win.mid.addstr("Finding entries...\n")
        self.win.mid.refresh()
        for e in editor.entries:
            self.win.mid.clear()

            to_add = True
            if len(self.selected_tags) > 0:
                to_add = False
                for t in e.tags:
                    if t in self.selected_tags:
                        to_add = True
                        break

            if self.search != "":
                to_add = (e.title.lower().find(self.search) != -1)

            if to_add:
                self.win.mid.addstr("Finding entries...\n(found {})".format(e.title))
                self.win.mid.refresh()
                self.entries.append(e)
                self.lst.append(e.title)

            for t in e.tags:
                if t != "" and not t in self.tags:
                    self.tags.append(t)

    def entry_to_dict(self, entry: Entry) -> bool:
        return {
            "title": entry.title,
            "credits": entry.credits,
            "dependencies": entry.dependencies.copy(),
            "tags": entry.tags.copy(),
            "flairs": entry.flairs.copy(),
            "flag": entry.flag,
            "description": entry.description
        }

    def check_entry_diff(self, entry: Entry, e_copy: dict) -> bool:
        return (
            entry.title != e_copy["title"]
            or entry.credits != e_copy["credits"]
            or entry.dependencies != e_copy["dependencies"]
            or entry.tags != e_copy["tags"]
            or entry.flairs != e_copy["flairs"]
            or entry.flag != e_copy["flag"]
            or entry.description != e_copy["description"]
        )

    def check_input(self, key: int) -> bool:
        if key == ord('r'):
            editor.load_entries(self.win.mid)
            self.update_entries()

        elif key == ord('n'):
            entry = Entry({}, "")
            e_copy = self.entry_to_dict(entry)
            EntryDetailsMenu(self.win, entry, self.tags).loop()
            self.win.clear_all()
            if (
                len(entry.title) != 0
                and len(entry.credits) != 0
                and self.check_entry_diff(entry, e_copy)
            ):
                tmp = editor.replace_unwanted_chars(entry.title) + ' - ' + editor.replace_unwanted_chars(entry.credits.replace("by ", ""))
                filename = self.create_textbox(self.create_floating_win(self.win.mid, (1, -1)), tmp)
                entry.filename = join('entries', filename if len(filename) > 0 else tmp) + ".yaml"
                self.win.mid.addstr("Saving entry...")
                self.win.mid.refresh()
                entry.save_entry()
                editor.load_entries(self.win.mid)
                self.win.mid.clear()
                self.update_entries()

            self.win.top.addstr("[Select entry]")
            self.win.bot.addstr(self.keybinds)
            self.win.refresh_all()

        elif key == ord('/'):
            self.win.bot.clear()
            self.win.bot.addstr("pick filter: [1] tags   | [2] search   | [/] clear filters")
            self.win.bot.refresh()
            key = self.win.mid.getch()

            if key == ord('/'):
                self.selected_tags.clear()
                self.search = ""
                self.update_entries()

            elif key == ord('1'):
                tmp = self.selected_tags.copy()
                menu = TagsMenu(self.win, self.tags, self.selected_tags)
                menu.loop()
                if self.selected_tags != tmp:
                    self.sel = 0
                    self.update_entries()

            elif key == ord('2'):
                prompt = "search: "
                self.win.bot.clear()
                self.win.bot.addstr(prompt)
                self.win.bot.refresh()
                editwin = self.win.bot.subwin(1, self.win.bot.getmaxyx()[1] - len(prompt), self.win.bot.getparyx()[0], len(prompt))

                newsearch = self.create_textbox(editwin, self.search).lower().strip() # for some reason, there's a random whitespace at the end of the string??
                if newsearch != self.search:
                    self.search = newsearch
                    self.update_entries()

            self.win.bot.clear()
            self.win.bot.addstr(self.keybinds)
            self.win.bot.refresh()

        else:
            return super().check_input(key)

        return True

    def draw(self) -> None:
        super().draw()

        if len(self.lst) == 0:
            self.win.mid.addstr("No entries found...\n(maybe change your filters?)")
            self.win.mid.refresh()

    def accept(self) -> None:
        entry = self.entries[self.sel]
        e_copy = self.entry_to_dict(entry)
        EntryDetailsMenu(self.win, entry, self.tags).loop()
        self.win.clear_all()
        if self.check_entry_diff(entry, e_copy):
            self.win.mid.addstr("Saving entry...")
            self.win.mid.refresh()
            entry.save_entry()
            self.win.mid.clear()
            self.update_entries()

        self.win.top.addstr("[Select entry]")
        self.win.bot.addstr(self.keybinds)
        self.win.refresh_all()
