from . Windows import Windows
from . BaseMenu import BaseMenu


class TagsMenu(BaseMenu):
    def __init__(self, win, tags: list, lst_to_edit: list):
        super().__init__(win)

        self.win = Windows(win.top, self.create_floating_win(win.mid), win.bot)
        self.lst = []
        self.selected_tags = lst_to_edit
        for i in tags:
            self.lst.append("[{}] {}".format("*" if i in self.selected_tags else " ", i))

    def accept(self) -> None:
        s = self.lst[self.sel][4:]
        selected = (s in self.selected_tags)
        self.lst[self.sel] = "[{}] {}".format(" " if selected else "*", s)

        if "" in self.selected_tags:
            self.selected_tags.remove("")

        if selected:
            self.selected_tags.remove(s)

        else:
            self.selected_tags.append(s)
