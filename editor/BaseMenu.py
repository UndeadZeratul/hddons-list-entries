import curses
from curses.textpad import Textbox, rectangle
from abc import ABC
from typing import Callable
import editor
from . Windows import Windows
from . TextEditor import TextEditor

class BaseMenu(ABC):
    def __init__(self, win: Windows):
        self.offset = 0
        self.sel = 0
        self.prevkey = 0
        self.win = win
        self.lst = []

    def handle_input(self) -> bool:
        key = self.win.mid.getch()
        return self.check_input(key)

    # returns False if input was ESC (or any key that sends you to the prev. menu)
    def check_input(self, key) -> bool:
        if key == curses.KEY_DOWN:
            self.sel += 1
            if self.sel >= len(self.lst):
                self.sel = 0
                self.offset = 0

            if self.offset + self.win.mid.getmaxyx()[0] < len(self.lst) and self.sel - self.offset >= self.win.mid.getmaxyx()[0] / 2:
                self.offset += 1

        elif key == curses.KEY_UP:
            self.sel -= 1
            if self.sel < 0:
                self.sel = len(self.lst) - 1
                self.offset = max(len(self.lst) - self.win.mid.getmaxyx()[0], 0)

            if self.offset != 0 and self.sel - self.offset < self.win.mid.getmaxyx()[0] / 2:
                self.offset -= 1

        elif key == editor.KEY_ENTER:
            self.accept()

        elif key == editor.KEY_ESC:
            return self.reject()

        return True

    def loop(self) -> None:
        while True:
            self.draw()

            if not self.handle_input():
                break

    def draw(self) -> None:
        self.win.mid.clear()
        index = self.offset
        for i in range(self.offset, len(self.lst)):
            s = self.lst[i]
            if len(s) >= self.win.mid.getmaxyx()[1]:
                s = s[:self.win.mid.getmaxyx()[1] - 3] + "..."

            if self.offset != 0 and index == self.offset:
                self.win.mid.addstr("...")

            elif index < len(self.lst) - 1 and self.win.mid.getyx()[0] + 1 >= self.win.mid.getmaxyx()[0]:
                self.win.mid.addstr("...")

            else:
                self.win.mid.addstr(
                    ("->| " if index == self.sel else "  | ") + s,
                    curses.A_STANDOUT if index == self.sel else curses.A_NORMAL
                )

            if self.win.mid.getyx()[0] + 1 >= self.win.mid.getmaxyx()[0]:
                break

            self.win.mid.addstr("\n")
            index += 1

        self.win.mid.refresh()

    def reject(self) -> bool:
        return False

    def accept(self) -> None:
        return

    def create_floating_win(self, win: curses.window, max_size: tuple[int, int]=(-1, -1)) -> curses.window:
        size = [
            int(win.getmaxyx()[0] * 0.75),
            int(win.getmaxyx()[1] * 0.50)
        ]
        if max_size[0] != -1:
            size[0] = min(size[0], max_size[0])

        if max_size[1] != -1:
            size[1] = min(size[1], max_size[1])

        offset = [
            int((win.getmaxyx()[0] - size[0]) / 2),
            int((win.getmaxyx()[1] - size[1]) / 2)
        ]

        editwin = win.subwin(size[0], size[1], offset[0], offset[1])
        rectangle(win, offset[0] - 2, offset[1] - 1, offset[0] + size[0] - 1, offset[1] + size[1] + 1)
        win.refresh()

        return editwin

    def create_textbox(self, editwin: curses.window, text: str, validator: Callable=None) -> str:
        try:
            curses.curs_set(1)

        except:
            self.win.bot.clear()
            self.win.bot.addstr("warning: visible cursor not supported... what")

        box = TextEditor(editwin, text)
        contents = box.edit(validator)

        try:
            curses.curs_set(0)

        except:
            self.win.bot.clear()
            self.win.bot.addstr("warning: invisible cursor not supported!")

        return contents
