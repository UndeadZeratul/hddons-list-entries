#!/usr/bin/env python3

# because i'm lazy to type a bunch of text to create files

import os
import pyperclip
import editor

def get_input(prompt):
	print(prompt)
	return input()


name = get_input("Name:")
cred = get_input("Credits:")

filename = editor.replace_unwanted_chars(name) + ' - ' + editor.replace_unwanted_chars(cred)

print("File name will be: " + filename)
choice = input("Edit? [y/N] ").lower()

if (choice == "y"):
	filename = get_input("Editing filename \"" + filename + "\"")

# Join the paths
target = os.path.join("entries/", filename + '.yaml')

# Get template
with open("template.yaml", "r") as f:
	template = f.read()

# Set name and credits
lines = template.split("\n")
lines[0] = '{}"{}"'.format(lines[0][0:-2], name.replace('"', '\\"'))
lines[1] = '{}"by {}"'.format(lines[1][0:-2], cred.replace('"', '\\"'))

# Paste description?
choice = input("Copy description from clipboard? [Y/n] ").lower()

if choice != "n":
	lines[10] = "    {}".format(pyperclip.paste().replace('\n', '\n    ').replace('    \n', '\n'))

# Write the file
with open(target, "w") as f:
	f.write("\n".join(lines))
	print("Wrote to " + target)
